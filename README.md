# Solve Phylogeny Problem with glpk and c++

- Minimum Character Removal (MCR)
- Imputing Missing Values (IMV)

## Details zur Lösung

Ich entschied mich für GLPK statt Gurobi, da GLPK trotz der fehlenden
Multiprozessor Unterstützung bei kleineren Problemen gute Lösungen
liefert. Wenn man nicht localhost als Hostname nutzt, scheint bei
der akademischen Gurobi Lizenz das "erneuern" nur für einen Tag zu
klappen. Zumindest habe ich jede Woche die selbe Meldung bzgl. der
flaschen hostid... und irgendwann ist das einfach nur nertötend die
Lizenz zu erneuern ohne erkennbaren Grund.

Anstelle einer kompletten Verarbeitung im C++ Code werden LP Dateien
als Zwischenergebnis erzeugt, welche dann wieder eingelesen werden.
Dies fand ich in Ordnung, da Aufgrund der Aufgabe Dateioperationen
eher den geringeren Zeitaufwand haben. Außerdem lassen sich so
die Dateien auf Korrektheit prüfen und auch im Notfall in
Gurobi einlesen.

Die Analyse der Eingabe, um ink. Charakter und potentiell ink. Charakter
zu finden ist recht unfein. Es werden Spalten und Zeilen sehr oft
mehrfach durchlaufen, um mögliche Inkompatibilität zu prüfen. Der Vorteil
ist, dass nur wichtige und zum Teil auch bereits vereinfachte Ungleichungen,
Gleichungen und Variablen an den Solver übergeben werden.

Um mit dem Auge leichter das Ergebnis mit der Eingabe zu vergleichen,
sind leere Spalte mit einem Leerfeld belegt. Dieser spezielle Teil der
Ausgabe ist mit den 2 Programmen imv_mcr_Solver und imvmcr_Solver
auch wieder einlesbar.

Hinweis: ab der Version vom 3. Januar (imvmcr_Solver.cpp) habe ich
die Presolver aktiviert. Es ist nun mit Instanz 03 ruck-zuck fertig.