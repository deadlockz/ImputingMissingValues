// g++ -Wall -lglpk imv_mcr_Solver.cpp -o imv_mcr_Solver

#include <glpk.h>
#include <cstdio>
#include <vector>
#include <map>
#include <utility> // pair
#include <fstream>

#define QUEST        '?'
#define IMV_LP_FILE "imv.lp"
#define MCR_LP_FILE "mcr.lp"
// column number 1332 = 4 Digits
#define MAX_CHARACTER_DIGITS 4

#define DEBUG  false
// timeout
#define MS_IMV 600000
#define MS_MCR 600000

using namespace std;

int m=0; // character
int n=0; // taxa
vector<pair<int,int> > ysilons;

FILE * fp;

vector< vector<bool> > mxn;  // m x n matrics 
vector< vector<bool> > isQ;  // is with questionmark
vector< vector<bool> > fail; // incompatible
vector< vector<int> >  ipot; // pot. incompatible (cause of >0 taxa/rows)
vector<bool>           hide; // charater 0..(m-1) to hide

map<int,bool> vertex;
vector< pair<int,int> * > edge;


bool inEdge(const int & a, const int & b) {
	for (auto v : edge) {
		if (v->first == a && v->second == b) {
			//printf("%d -> %d exists\n", a, b);
			return true;
		}
	}
	return false;
}

int strToInt(char *str, int leng) {
	int pot = 1;
	int res = 0;
	
	while (leng > 0) {
		res += pot * (int)(str[leng-1] - '0');
		leng--;
		pot *= 10;
	}
	return res;
}

int strToInt(const char * str) {
	int pot = 1;
	int res = 0;
	int leng =0;
	while (str[leng] != '\0') leng++;
	
	while (leng > 0) {
		res += pot * (int)(str[leng-1] - '0');
		leng--;
		pot *= 10;
	}
	return res;
}

pair<int,int> * extractPair(const char * varName) {
	int a = 0;
	int b = 0;
	char * num1 = new char[MAX_CHARACTER_DIGITS+1](); // () makes "\0\0..."
	char * num2 = new char[MAX_CHARACTER_DIGITS+1]();
	int pos = 2;
	int i=0;
	int j=0;
	bool frst = true;
	char c = varName[pos];
	while (c != 0) {
		if (c == '_') {
			frst = false;
			c = varName[++pos];
			continue;
		}
		
		if (frst) {
			num1[i++] = c;
		} else {
			num2[j++] = c;
		}
		c = varName[++pos];
	}
	a = strToInt(num1, i);
	b = strToInt(num2, j);
	
	delete[] num2;
	delete[] num1;
	return new pair<int,int>(a,b);
}

/**
 * "i_xx_yy" put xx and yy to vertex (index)=true
 *           put xx->yy to edge as pair
 */
void fill_E_V(const char * varName) {
	pair<int,int> * p = extractPair(varName);
	edge.push_back(p);
	vertex[p->first]  = true;
	vertex[p->second] = true;
}

bool readPylogeny(char * filename) {
	char ch;
	fstream fin(filename, fstream::in);
	// statistics
	while (fin >> noskipws >> ch) {
		if (ch == ' ') {
			//ignore;
		} else if (ch == '1') {
			m++;
		} else if (ch == '0') {
			m++;
		} else if (ch == QUEST) {
			ysilons.push_back(pair<int,int>(n,m));
			m++;
		} else if (ch == '\n') {
			if (m==0) {
				fprintf(stderr, "fail: please remove (file ending?) empty lines!\n");
				return false;
			}
			m=0;
			n++;
		} else {
			fprintf(stderr, "fail: line %d, character %d is %c\n", n+1, m, ch);
			return false;
		}
	}
	n++;
	
	printf("read %d taxa, %d characters\n", n, m);
	
	// set matrix size
	mxn.resize(m);
	isQ.resize(m);
	fail.resize(m);
	ipot.resize(m);
	
	for (int i=0; i<m; ++i) {
		mxn[i].resize(n);
		isQ[i].resize(n, false);
		
		fail[i].resize(m, false);
		ipot[i].resize(m, 0);
	}
	hide.resize(m, false);
	
	m=0;
	n=0;
	
	fstream fin2(filename, fstream::in);
	// fill data
	while (fin2 >> noskipws >> ch) {
		if (ch == ' ') {
			//ignore;
		} else if (ch == '1') {
			mxn[m][n] = true;
			m++;
		} else if (ch == '0') {
			mxn[m][n] = false;
			m++;
		} else if (ch == QUEST) {
			mxn[m][n] = false; // dummy
			isQ[m][n] = true;
			m++;
		} else {
			m=0;
			n++;
		}
	}
	n++;
	return true;
}

void printData() {
	for (int i=0; i<n; ++i) {
		for (int j=0; j<m; ++j) {
			if (isQ[j][i]) {
				printf(".");
			} else {
				if (mxn[j][i])
					printf("1");
				else
					printf("o");
			}
		}
		printf("\n");
	}
}

void printIncompatible(vector< vector<bool> > data, char c) {
	char * str;
	bool isEmpty=true;
	for (int p=0; p<m; ++p) {
		isEmpty = true;
		str = new char[m+1];
		str[m] = '\0';
		for (int q=0; q<m; ++q) {
			if (data[p][q]) {
				str[q] = c;
				isEmpty=false;
			} else {
				str[q] = ' ';
			}
		}
		str[p] = 'x';
		if (isEmpty == false) printf("%s\n", str);
		delete[] str;
	}
}

void printIncompatible(vector< vector<int> > data, char c) {
	char * str;
	bool isEmpty=true;
	for (int p=0; p<m; ++p) {
		isEmpty = true;
		str = new char[m+1];
		str[m] = '\0';
		for (int q=0; q<m; ++q) {
			if (data[p][q]>0) {
				str[q] = c;
				isEmpty=false;
			} else {
				str[q] = ' ';
			}
		}
		str[p] = 'x';
		if (isEmpty == false) printf("%s\n", str);
		delete[] str;
	}
}

void printY() {
	for (auto & pa : ysilons) {
		fprintf(fp, "y_%d_%d ", pa.first+1, pa.second+1);
	}
}

void printB() {
	for (int p=0; p<(m-1); ++p) {
		for (int q=(p+1); q<m; ++q) {
			if (fail[p][q] == false) {
				// I need B vars
				fprintf(fp, "B_%d_%d_01 ", p+1, q+1);
				fprintf(fp, "B_%d_%d_10 ", p+1, q+1);
				fprintf(fp, "B_%d_%d_11 ", p+1, q+1);
			}
		}
	}
}

void printI() {
	for (int p=0; p<(m-1); ++p) {
		for (int q=(p+1); q<m; ++q) {
			fprintf(fp, "i_%d_%d ", p+1, q+1);
		}
	}
}

void printObjective() {
	fprintf(fp, "minimize\n  obj1: ");
	for (int p=0; p<(m-1); ++p) {
		for (int q=(p+1); q<m; ++q) {
			fprintf(fp, "i_%d_%d", p+1, q+1);
			if ( ! (((p+2)==m) && ((q+1)==m)) ) fprintf(fp, " + ");
		}
	}
	fprintf(fp, "\n");
}

void printMcrObjective() {
	fprintf(fp, "maximize\n  obj1: ");
	unsigned int i=1;
	for (auto v : vertex) {
		fprintf(fp, "x_%d", v.first);
		if (i != vertex.size()) fprintf(fp, " + ");
		i++;
	}
	fprintf(fp, "\n");
}

void printX() {
	for (auto v : vertex) {
		fprintf(fp, "x_%d ", v.first);
	}
}

void createX(int & cond) {
	for (auto v1 : vertex) {
		for (auto v2 : vertex) {
			if (v1.first < v2.first && inEdge(v1.first, v2.first)) {
				fprintf(fp, "co%d: x_%d + x_%d <= 1\n", cond++, v1.first, v2.first);
			}
		}
	}
}


int hasY11(int & cond, int p, int q) {
	int be = 0;
	for (int row=0; row<n; ++row) {
		// ?1
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1);
			be = 1;
		}
		// 1?
		if (
			(isQ[p][row] == false) &&
			(isQ[q][row] == true) &&
			(mxn[p][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1);
			be = 1;
		}
		// ??
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d - y_%d_%d >= -1\n", cond++, p+1, q+1, row+1, p+1, row+1, q+1);
			be = 1;
		}
	}
	
	if (be == 0) {
		// could be with 0? or ?0
		fprintf(fp, "co%d: B_%d_%d_11 = 0\n", cond++, p+1, q+1);
	}
	return be;
}

int hasY10(int & cond, int p, int q) {
	int be = 0;
	for (int row=0; row<n; ++row) {
		// ?0
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == false)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1);
			be = 1;
		}
		// 1?
		if (
			(isQ[p][row] == false) &&
			(isQ[q][row] == true) &&
			(mxn[p][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 + y_%d_%d >= 1\n", cond++, p+1, q+1, row+1, q+1);
			be = 1;
		}
		// ??
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 - y_%d_%d + y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1, row+1, q+1);
			be = 1;
		}
	}
	
	if (be == 0) {
		// could be with 0?
		fprintf(fp, "co%d: B_%d_%d_10 = 0\n", cond++, p+1, q+1);
	}
	return be;
}

int hasY01(int & cond, int p, int q) {
	int be = 0;
	for (int row=0; row<n; ++row) {
		// ?1
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 + y_%d_%d >= 1\n", cond++, p+1, q+1, row+1, p+1);
			be = 1;
		}
		// 0?
		if (
			(isQ[p][row] == false) &&
			(isQ[q][row] == true) &&
			(mxn[p][row] == false)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1);
			be = 1;
		}
		// ??
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 - y_%d_%d + y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1, row+1, p+1);
			be = 1;
		}
	}
	
	if (be == 0) {
		// could be with ?0
		fprintf(fp, "co%d: B_%d_%d_01 = 0\n", cond++, p+1, q+1);
	}
	return be;
}


int hasY0111(int & cond, int p, int q) {
	bool has01 = false;
	bool has11 = false;
	
	// Q -> 01+11 ?
	for (int row=0; row<n; ++row) {
		// 0?
		if (
			(isQ[p][row] == false) &&
			(isQ[q][row] == true) &&
			(mxn[p][row] == false)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1);
			has01=true;
		}
		
		// ?1
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 + y_%d_%d >= 1\n", cond++, p+1, q+1, row+1, p+1);
			has01=true;
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1);
			has11=true;
		}
		
		// ??
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 - y_%d_%d + y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1, row+1, p+1);
			has01=true;
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d - y_%d_%d >= -1\n", cond++, p+1, q+1, row+1, p+1, row+1, q+1);
			has11=true;
		}
	}
	
	// we do not have to count the ?? lines (need 2), because we check this in the loop for building ipot-Array !
	
	return (has01 && has11)? 1 : 0;
}


int hasY1011(int & cond, int p, int q) {
	bool has10 = false;
	bool has11 = false;
	
	// Q -> 10+11 ?
	for (int row=0; row<n; ++row) {
		
		// 1?
		if (
			(isQ[p][row] == false) &&
			(isQ[q][row] == true) &&
			(mxn[p][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 + y_%d_%d >= 1\n", cond++, p+1, q+1, row+1, q+1);
			has10=true;
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1);
			has11=true;
		}
		
		// ?1
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1);
			has11=true;
		}
		
		// ?0
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == false)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1);
			has10=true;
		}
		
		// ??
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 - y_%d_%d + y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1, row+1, q+1);
			has10=true;
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d - y_%d_%d >= -1\n", cond++, p+1, q+1, row+1, p+1, row+1, q+1);
			has11=true;
		}
	}
	
	// we do not have to count the ?? lines (need 2), because we check this in the loop for building ipot-Array !
	
	return (has10 && has11)? 1 : 0;
}


int hasY1001(int & cond, int p, int q) {
	bool has10 = false;
	bool has01 = false;
	
	// Q -> 10+01 ?
	for (int row=0; row<n; ++row) {
		
		
		// 1?
		if (
			(isQ[p][row] == false) &&
			(isQ[q][row] == true) &&
			(mxn[p][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 + y_%d_%d >= 1\n", cond++, p+1, q+1, row+1, q+1);
			has10=true;
		}
		
		// ?1
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 + y_%d_%d >= 1\n", cond++, p+1, q+1, row+1, p+1);
			has01=true;
		}
		
		// ?0
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == false)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1);
			has10=true;
		}
		
		// ??
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 - y_%d_%d + y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1, row+1, q+1);
			has10=true;
			fprintf(fp, "co%d: B_%d_%d_01 - y_%d_%d + y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1, row+1, p+1);
			has01=true;
		}
	}
	
	// we do not have to count the ?? lines (need 2), because we check this in the loop for building ipot-Array !
	
	return (has10 && has01)? 1 : 0;
}





int hasIncViaYonly(int & cond, int p, int q) {
	bool has01 = false;
	bool has10 = false;
	bool has11 = false;
	
	for (int row=0; row<n; ++row) {
		// 0?
		if (
			(isQ[p][row] == false) &&
			(isQ[q][row] == true) &&
			(mxn[p][row] == false)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1);
			has01=true;
		}
		
		// 1?
		if (
			(isQ[p][row] == false) &&
			(isQ[q][row] == true) &&
			(mxn[p][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 + y_%d_%d >= 1\n", cond++, p+1, q+1, row+1, q+1);
			has10=true;
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1);
			has11=true;
		}
		
		// ?1
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 + y_%d_%d >= 1\n", cond++, p+1, q+1, row+1, p+1);
			has01=true;
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1);
			has11=true;
		}
		
		// ?0
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == false) &&
			(mxn[q][row] == false)
		) {
			fprintf(fp, "co%d: B_%d_%d_10 - y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1);
			has10=true;
		}
		
		// ??
		if (
			(isQ[p][row] == true) &&
			(isQ[q][row] == true)
		) {
			fprintf(fp, "co%d: B_%d_%d_01 - y_%d_%d + y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, q+1, row+1, p+1);
			has01=true;
			fprintf(fp, "co%d: B_%d_%d_10 - y_%d_%d + y_%d_%d >= 0\n", cond++, p+1, q+1, row+1, p+1, row+1, q+1);
			has10=true;
			fprintf(fp, "co%d: B_%d_%d_11 - y_%d_%d - y_%d_%d >= -1\n", cond++, p+1, q+1, row+1, p+1, row+1, q+1);
			has11=true;
		}
	}

	// we do not have to count the ?? lines (need 2) or others, because we check this in
	//the loop for building ipot-Array !
	
	return (has10 && has01 && has11)? 1 : 0;
}



void createIB(int & cond) {
	for (int p=0; p<m; ++p) {
		for (int q=0; q<m; ++q) {
			// incompatible via const and without '?'
			if (fail[p][q]) {
				fprintf(fp, "co%d: i_%d_%d = 1\n", cond++, p+1, q+1);
				continue;
			}

			if (ipot[p][q]>0) {
				bool has10 = false;
				bool has01 = false;
				bool has11 = false;
				bool hasQ  = false;
				
				//printf("(p,q) %d %d\n", p+1, q+1);
				
				for (int row=0; row<n; ++row) {
					
					// ignore 00
					if (
						(isQ[p][row] == false) &&
						(isQ[q][row] == false) &&
						(mxn[p][row] == false) &&
						(mxn[q][row] == false)
					) continue;
					
					// const 10
					if (
						has10 == false &&
						(isQ[p][row] == false) &&
						(isQ[q][row] == false) &&
						(mxn[p][row] == true) &&
						(mxn[q][row] == false)
					) {
						fprintf(fp, "co%d: B_%d_%d_10 = 1\n", cond++, p+1, q+1);
						has10 = true;
						continue;
					}
					// const 01
					if (
						has01 == false &&
						(isQ[p][row] == false) &&
						(isQ[q][row] == false) &&
						(mxn[p][row] == false) &&
						(mxn[q][row] == true)
					) {
						fprintf(fp, "co%d: B_%d_%d_01 = 1\n", cond++, p+1, q+1);
						has01 = true;
						continue;
					}
					// const 11
					if (
						has11 == false &&
						(isQ[p][row] == false) &&
						(isQ[q][row] == false) &&
						(mxn[p][row] == true) &&
						(mxn[q][row] == true)
					) {
						fprintf(fp, "co%d: B_%d_%d_11 = 1\n", cond++, p+1, q+1);
						has11 = true;
						continue;
					}
					
					if (isQ[p][row]) {
						//printf( "  Q");
						hasQ = true;
					} else {
						if (mxn[p][row]) {
							//printf("  1");
						} else {
							//printf("  0");
						}
					}
					
					if (isQ[q][row]) {
						//printf("Q");
						hasQ = true;
					} else {
						if (mxn[q][row]) {
							//printf("1");
						} else {
							//printf("0");
						}
					}
					
					
					//printf("\n");
					
				} // end run through rows ------
				
				int bees = 0;
				if (hasQ) {
					if (has01 && has10) {
						// Q -> 11 ?
						bees += hasY11(cond,p,q);
						
					} else if (has01 && has11) {
						// Q -> 10 ?
						bees += hasY10(cond,p,q);
						
					} else if (has10 && has11) {
						// Q -> 01 ?
						bees += hasY01(cond,p,q);
						
					} else if (has10) {
						// Q -> 01+11 ?
						bees += hasY0111(cond,p,q);
						
					} else if (has01) {
						// Q -> 10+11 ?
						bees += hasY1011(cond,p,q);
						
					} else if (has11) {
						// Q -> 10+01 ?
						bees += hasY1001(cond,p,q);
						
					} else {
						//no const. -> I need to build 01+10+11 with ysilons
						bees += hasIncViaYonly(cond,p,q);
						
					}
					
					if (bees > 0) {
						fprintf(
							fp,
							"co%d: i_%d_%d - B_%d_%d_01 - B_%d_%d_10 - B_%d_%d_11 >= -2\n", cond++,
							p+1, q+1,
							p+1, q+1,
							p+1, q+1,
							p+1, q+1
						);
					} else {
						// not in incompatible via const and there is no '?' pair to make it incompatible
						fprintf(fp, "co%d: i_%d_%d = 0\n", cond++, p+1, q+1);
					}
				} else {
					// not in incompatible via const and there is no '?'
					fprintf(fp, "co%d: i_%d_%d = 0\n", cond++, p+1, q+1);
				}
				
			}
		}
	}
}

void checkColumns() {
	int p,q,row;
	
	int is01 = 0;
	int is10 = 0;
	int is11 = 0;
	
	int isQ0 = 0;
	int isQ1 = 0;
	int isQQ = 0;

	int is0Q = 0;
	int is1Q = 0;
	
	for (p=0; p<(m-1); ++p) { // check column p with ...
		for (q=(p+1); q<m; ++q) { // ... column q
			is01 = 0;
			is10 = 0;
			is11 = 0;
			
			isQ0 = 0;
			isQ1 = 0;
			isQQ = 0;
			
			is0Q = 0;
			is1Q = 0;
			for (row=0; row<n; ++row) {
				if (isQ[p][row] == true) {
					
					if (isQ[q][row] == true) {
						isQQ++;
						
					} else {
						if (mxn[q][row] == true) {
							isQ1++;
							
						} else {
							isQ0++;
						}
					}
					
				} else if (mxn[p][row] == true) {
					
					if (isQ[q][row] == true) {
						is1Q++;
						
					} else if (mxn[q][row] == true) {
						is11++;
						
					} else {
						is10++;
					}
					
				} else {
					
					if (isQ[q][row] == true) {
						is0Q++;
						
					} else if (mxn[q][row] == true) {
						is01++;
					}
					
				}
				
				// ---------------------------------------------
				
				if (is01>0 && is10>0 && is11>0) {
					// @todo how to handle this?
					fail[p][q] = true;
				}
				
				if (is01>0 && is10>0 && ( isQ1>0 || is1Q>0 || isQQ>0 )) {
					// fail on 11
					ipot[p][q]++;
				}

				if (is01>0 && ( is1Q>0 || isQ0>0 || isQQ>0 ) && is11>0) {
					// fail on 10
					ipot[p][q]++;
				}

				if ( ( is0Q>0 || isQ1>0 || isQQ>0 ) && is10>0 && is11>0) {
					// fail on 01
					ipot[p][q]++;
				}

				if ( 
					(
						( (is0Q>0 || isQ1>0) && isQQ>0 ) ||
						( (is1Q>0 || isQ0>0) && isQQ>0 ) ||
						( (is0Q>0 || isQ1>0) && (is1Q>0 || isQ0>0) ) ||
						isQQ>1
					) && 
					is11>0
				) {
					// fail on 01+10
					ipot[p][q]++;
				}

				if ( 
					(
						  (is1Q>0 && isQ0>0) ||
						( (is1Q>0 || isQ0>0) && isQQ>0 ) ||
						isQQ>1
					) && 
					is01>0
				) {
					// fail on 10+11
					ipot[p][q]++;
				}

				if ( 
					(
						  (is0Q>0 && is1Q>0) ||
						( (is0Q>0 || is1Q>0) && isQQ>0 ) ||
						isQQ>1
					) && 
					is10>0
				) {
					// fail on 01+11
					ipot[p][q]++;
				}

				// just '?' - no const in rows like 01,10 or 11
				if (
					(is0Q>0 && is1Q>0 && isQQ>0) ||
					(is0Q>0 && is1Q>1) ||

					(isQ0>0 && isQ1>0 && isQQ>0) ||
					(isQ0>0 && isQ1>1) ||

					(isQQ>1 && isQ1>0) ||
					(isQQ>1 && isQ0>0) ||
					(isQQ>1 && is0Q>0) ||
					(isQQ>1 && is1Q>0) ||

					isQQ>2
				) {
					// fail on 01+10+11
					ipot[p][q]++;
				}

			} // end row checking
			
		} // end q loop
	} // end p loop
}


// y_rowTaxa_columnCharacter
void writeQ(const char * varName, bool val) {
	pair<int,int> * p = extractPair(varName);
	isQ[p->second-1][p->first-1] = false;
	mxn[p->second-1][p->first-1] = val;
	delete p;
}

void writeResult() {
	for (int i=0; i<n; ++i) { // taxas
		for (int j=0; j<m; ++j) { // characters
			if (hide[j]) {
				printf(" ");
			} else {
				if (mxn[j][i])
					printf("1");
				else
					printf("0");
			}
		}
		if (i<(n-1)) printf("\n");
	}
}


int main(int argc, char **argv) {
	if (argc != 2) {
		fprintf(stderr, "file missing\n");
		return -1;
	}

	if (readPylogeny(argv[1]) == false) {
		return -2;
	}
	
	printf("%lu unsure values\n", ysilons.size());
	
	printData();
	printf("\n");
	
	checkColumns();
	
	printf("incompatible\n");
	printIncompatible(fail, 'F');
	printf("\n");
	printf("pot. incompatible\n");
	printIncompatible(ipot, '?');
	printf("\n");
	
	// -----------------------
	fp = fopen(IMV_LP_FILE, "w");
	
	printObjective();
	fprintf(fp, "\n");
	fprintf(fp, "subject to\n");
	int cond = 0;
	createIB(cond);

	fprintf(fp, "\n");
	fprintf(fp, "binary\n    ");
	printY();
	fprintf(fp, "\n    ");
	printB();
	fprintf(fp, "\n    ");
	printI();
	fprintf(fp, "\n");
	fprintf(fp, "end\n");
	
	fclose(fp);
	// -----------------------
 
	glp_prob *lp;
	lp = glp_create_prob();
	if (!DEBUG) glp_term_out(GLP_OFF);

	glp_read_lp(lp, NULL, IMV_LP_FILE);
	glp_smcp para1;
	glp_init_smcp(&para1);
	glp_simplex(lp, &para1);

	glp_iocp para2;
	glp_init_iocp(&para2);
	para2.tm_lim = MS_IMV;
	glp_intopt(lp, &para2);

	printf("Best solution: %d\n", (int) glp_mip_obj_val(lp));

	int columns = glp_get_num_cols(lp);
	const char * varName;
	for (int n=0; n < columns; ++n) {
		varName = glp_get_col_name(lp, n+1);
		
		if (varName[0] == 'i' && glp_mip_col_val(lp, n+1) > 0) {
			printf("  hide pair %10s\n", varName);
			fill_E_V(varName);
		}
		
		if (varName[0] == 'y') {
			writeQ(varName, glp_mip_col_val(lp, n+1)>0);
			printf("%10s = %d\n", 
				varName,
				glp_mip_col_val(lp, n+1)>0 ? 1 : 0
			);
		}
	}
	glp_delete_prob(lp);
	
	// -------------------------------------------------------
	
	printf("These %ld of %d characters makes trouble:\n", vertex.size(), m);
	for (auto v : vertex) {
		printf("%d ", v.first);
	}
	printf("\n");
	
	// -----------------------
	fp = fopen(MCR_LP_FILE, "w");
	
	printMcrObjective();
	fprintf(fp, "\n");
	fprintf(fp, "subject to\n");
	cond = 0;
	createX(cond);
	fprintf(fp, "\n");
	fprintf(fp, "binary\n    ");
	printX();
	fprintf(fp, "\n");
	fprintf(fp, "end\n");
	
	fclose(fp);
	// -----------------------
	
	lp = glp_create_prob();
	if (!DEBUG) glp_term_out(GLP_OFF);

	glp_read_lp(lp, NULL, MCR_LP_FILE);
	glp_init_smcp(&para1);
	glp_simplex(lp, &para1);
	glp_init_iocp(&para2);
	para2.tm_lim = MS_MCR;
	glp_intopt(lp, &para2);

	printf("Leave %d trouble characters untouched\n", (int) glp_mip_obj_val(lp));

	columns = glp_get_num_cols(lp);
	int rem=1;
	for (int n=0; n < columns; ++n) {
		varName = glp_get_col_name(lp, n+1);
		if ( glp_mip_col_val(lp, n+1)<1 ) {
			int ch = strToInt(&(varName[2]));
			printf("  %4d. remove %d\n", rem++, ch);
			hide[ch-1] = true;
		}
	}
	
	for (auto v : edge) delete v;
	
	writeResult();
	
	glp_delete_prob(lp);
	return 0;
}
